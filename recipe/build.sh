#!/bin/bash

cp scripts/propka31.py propka/__main__.py
python setup.py install \
    --single-version-externally-managed \
    --record=record.txt \
    --install-scripts="$PREFIX/bin"
